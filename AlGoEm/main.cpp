﻿// AlGoEm.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.


#include <iostream>
#include <Windows.h>
#include "Clothes.h"
using namespace std;
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int YorN, n, s = 0;

	cout << endl << " Произвести ввод данных вручную (1) или вывести готовые записи с параметрами (2)? " << endl << endl;
	cin >> YorN;
	while ((2 < YorN) || (YorN < 1)) 
	{
		cout << " Невозможно выбрать данный вариант!" << endl << " Произвести ввод данных вручную (1) или вывести готовые записи с параметрами (2)?" << endl;
		cin >> YorN;
	}
	
	if (YorN == 1) 
	{
		cout << endl << " Пожалуйста, введите необходимое количество записей:" << endl;
		cin >> n;
		while ((n <= 0) || (n > 5))
		{
			cout << " Невозможный размер массива!" << endl << " Пожалуйста, введите необходимое количество записей: " << endl;
			cin >> n;
		}

		Clothes warehouse[5];
		for (int i = 0; i < n; i++)
		{
			warehouse[i].input();
			system("cls");
		}

		cout << "| Название бренда     " << "| Тип изделия        " << " | Материал           " << " | Размер             " << " | Цена                |" << endl;
		for (int i = 0; i < n; i++)
		{
			warehouse[i].output();
		}
			cout << endl << "< Нажмите любую клавишу для продолжения или завершения программы >" << endl;
			system("PAUSE>nul");
	}
	if (YorN == 2) 
	{
		system("cls");
		cout << endl << "   ЗАПИСЬ С НЕПОЛНЫМИ ПАРАМЕТРАМИ: " << endl << endl;
		cout << "| Название бренда     " << "| Тип изделия        " << " | Материал           " << " | Размер             " << " | Цена                |" << endl;
		Clothes two("джемперы", "42", 1799);
		two.output();
		cout << endl << endl << "   ЗАПИСЬ С ПОЛНЫМИ ПАРАМЕТРАМИ: " << endl << endl;
		cout << "| Название бренда     " << "| Тип изделия        " << " | Материал           " << " | Размер             " << " | Цена                |" << endl;
		Clothes three("Levi's", "комбинезон", "лиоцелл", "48", 2420);
		three.output();
		cout << endl << "< Для выхода из программы нажмите любую клавишу >" << endl;
		system("PAUSE>nul");
	}
}
