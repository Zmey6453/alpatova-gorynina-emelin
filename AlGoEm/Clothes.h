#pragma once

//16. ����� ������� (��������, ���, ��������, ������, ���� � �.�)
class Clothes 
{
public:
	Clothes();
	Clothes(const char *Ptype, const char *Psize, int Pprice);
	Clothes(const char *PbrandName, const char *Ptype, const char *Pmaterial, const char *Psize, int Pprice);
	~Clothes();

	void input();
	void output();

	char *brandName;
	char *type;
	char *size;
	
protected:
	void init();
	char *material;

private:
	int price;
};


