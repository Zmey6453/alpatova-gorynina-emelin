#include "Clothes.h"
#include <iostream>
using namespace std;

#define _CRT_SECURE_NO_WARNINGS

int YorN, YorN2;
void Clothes::init() {
	brandName = new char[20];
	type = new char[20];
	material = new char[20];
	size = new char[3];


}

Clothes::Clothes() {
	init();

	strcpy_s(brandName, 20, "-");
	strcpy_s(type, 20, "-");
	strcpy_s(material, 20, "-");
	strcpy_s(size, 3, "-");
	price = 0;
}

Clothes::Clothes(const char *Ptype, const char *Psize, int Pprice) {
	init();

	strcpy_s(brandName, 20, "-");
	strcpy_s(type, 20, Ptype);
	strcpy_s(material, 20, "-");
	strcpy_s(size, 3, Psize);
	price = Pprice;
}


Clothes::Clothes(const char *PbrandName, const char *Ptype, const char *Pmaterial, const char *Psize, int Pprice) {
	init();

	strcpy_s(brandName, 20, PbrandName);
	strcpy_s(type, 20, Ptype);
	strcpy_s(material, 20, Pmaterial);
	strcpy_s(size, 3, Psize);
	price = Pprice;
}


Clothes::~Clothes() {
	delete brandName;
	delete type;
	delete material;
	delete size;
	price = -1;
}


void Clothes::input() {
	cout << endl;
	cout << " ������� �������� ������: " << endl;
	cin >> brandName;

	cout << " ������� ��� �������: " << endl;
	cin >> type;

	cout << " ������� �������� �������: " << endl;
	cin >> material;

	cout << " ������� ������ �������: " << endl;
	cin >> size;

	cout << " ������� ���� �������: " << endl;
	cin >> price;

	cout << endl << " ������ ���������" << endl << " ���������� (1) ��� ������ ��������� (2)?" << endl;
	cin >> YorN;

	while ((2 < YorN) || (YorN < 1)) {
		if ((2 < YorN) || (YorN < 1)) {

			cout << " ���������� ������� ������ �������!" << endl << " ���������� (1) ��� ������ ��������� (2)?" << endl;
			cin >> YorN;
		}
	}

	if (YorN == 2)
	{
		cout << " �������� ��������� ��� ��������: ����� (1), ��� (2), �������� (3), ������ (4), ���� (5)" << endl;
		cin >> YorN2;
		while ((5 < YorN2) || (YorN2 < 1)) {
			if ((5 < YorN2) || (YorN2 < 1))
			{
				cout << " ���������� ������� ������ �������!" << endl << " �������� ��������� ��� ���������: ����� (1), ��� (2), �������� (3), ������ (4), ���� (5)" << endl;
				cin >> YorN2;
			}
		}
		switch (YorN2)
		{
		case 1: 
			{
				cout << " ������� �������� ������: " << endl;
				cin >> brandName;
				break;
			}
		case 2:
			{
				cout << " ������� ��� �������: " << endl;
				cin >> type;
				break;
			}
		case 3:
			{
				cout << " ������� �������� �������: " << endl;
				cin >> material;
				break;
			}
		case 4:
			{
				cout << " ������� ������ �������: " << endl;
				cin >> size;
				break;
				}
		case 5:
			{
				cout << " ������� ���� �������: " << endl << endl;
				cin >> price;
				break;
			}
		}
	}
}

void Clothes::output() {

	//cout << "| �������� ������     " << "| ��� �������        " << " | ��������           " << " | ������             " << " | ����                |" << endl;
	cout.setf(ios::left);
	cout << "| ";
	cout.width(20);
	cout << brandName;
	cout.setf(ios::left);
	cout << "| ";
	cout.width(20);
	cout << type;
	cout.setf(ios::left);
	cout << "| ";
	cout.width(20);
	cout << material;
	cout.setf(ios::left);
	cout << "| ";
	cout.width(20);
	cout << size;
	cout.setf(ios::left);
	cout << "| ";
	cout.width(20);
	cout << price;
	cout << "| " << endl;

}